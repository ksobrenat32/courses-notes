package main

import (
	"fmt"
	"strconv"
)

// We can also declare variables from outside of main function
var _d float32 = 45

// We can declare multiple variables in a single 'var'
var (
	_e string = "This is a good _e string"
	_f string = "I am ksobrenatural"
)

// Or have multiple 'var', this helps to have clean code
var (
	_g int     = 45
	_h float64 = 46.
)

func main() {
	// To declare a variable, we use 'var variable_name type' in this case int.
	// This is useful if we need to declare it but we do not need to give it a value yet.
	var _a int
	_a = 42

	// We can also declare a variable with a starting value in the same line
	var _b float32 = 43

	// We can even not specify the type so go identify it, buit this will only decide between int, float64 or string.
	// This is not always recomended, it makes it less clear
	_c := 44.

	// Use println to have '\n' newline by default
	fmt.Println("These are the declared variables")
	// Use with printf, '%v' to print the value of the variable and '%T' for the type
	fmt.Printf("%v -	_a's type is %T\n", _a, _a)
	fmt.Printf("%v -	_b's type is %T\n", _b, _b)
	fmt.Printf("%v -	_c's type is %T\n", _c, _c)
	fmt.Printf("%v -	_d's type is %T\n", _d, _d)
	fmt.Printf("%v -	_e's type is %T\n", _e, _e)
	fmt.Printf("%v -	_f's type is %T\n", _f, _f)
	fmt.Printf("%v -	_g's type is %T\n", _g, _g)
	fmt.Printf("%v -	_h's type is %T\n", _h, _h)

	// We can make a conversion between variables
	var _k float32
	_k = float32(_a)
	fmt.Printf("%v -	_k's type is %T\n", _k, _k)

	// For converting a number to a string
	var _l string
	_l = strconv.Itoa(_a)
	fmt.Printf("%v -	_l's type is %T\n", _l, _l)
}
