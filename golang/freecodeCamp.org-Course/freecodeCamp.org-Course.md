## Introduction

My notes and exercises from the video course for begginers from freecodecamp.

Video Course link: https://www.youtube.com/watch?v=YS4e4q9oBaU

### Go(golang) characteristics

- **Is strong and statically typed**, this means the variable type can not change over time and they have to be declared at compile time.
- **Good community.**
- **Key features**
    - **Simplicity**, not a lot features that make complicated code.
    - **Fast compile times.**
    - **Garbage collected**, you do not have to manage memory.
    - **Built-in concurrency**, no library needed
    - **Compile to standalone binaries**, all go dependencies are contained

- [Official webpage](https://golang.org/): golang playgrounf, downloading go, and documentation.

### Basic structure

[Go online playgroung](https://play.golang.org/)

```go
    package main    // Place to declare go packages
                    // In this case main is the entrie point 

    import (
        "fmt"       // Import library in this case "fmt", this allows to format strings
    )

    func main() {   // Our aplication entriepoint, where the applications go
        fmt.Println("Hello, playground")
    }
```

### Variables

To declare a variable, we use `var variable_name type` in this case int. This is useful if we need to declare it but we do not need to give it a value yet.We can also declare a variable with a starting value in the same line `variable_name = value`. We can even not specify the type so go identify it `variable_name := value`, but this will only decide between int, float64 or string (This is not always recomended, it makes it less clear).

We can declare them both inside or outside the function main and at a block level but not both at the same time, we can change the value (shadowing) but not redeclare it.

If the code has a variable that is declared but it is not used, the build will fail.

There are three levels of variable visibility:
- **Any file in the same package can access the variable**  - If you have it at a package level and is lowercase
- **Exported and globally**  - If you have it at a package level and is uppercase
- **Block level**  - If you have it in block 

Some conventions on variable naming:
- The length of the variable name, should rename the lifespan of the variable.
- If the variable will be used in a package, make them verbose, clear and understandable.
- Keep acronyms in uppercase for readibility

We can convert the value of a variable to another type but not in the same variable, we need to declare a second variable to which you assign the value, example:

``` go
    var i int = 10 // the value of i is 10 and an integer
    var j float32 // declare j variable
    j = float32(i) // the value of j is the same as i but it is a float32
```

We cannot directly convert from numbers to string, we would need to import `strconv` and now we can do it.

``` go
    var i int = 10 // the value of i is 10 and an integer
    var j string // declare j variable
    j = strconv.Itoa(i) // the value of j is the same as i but we use strvon,Itoa to convert it
```

### Primitives

**Boolean** : It can only be true or false, it is initialized with `var name bool = true/false`. They are mostly used as state flag, or in the result of logical tests ( something like this `m := 1 == 2`). Everytime you initialize a varibale it has a *zero value*, in booleans is false.

**Value** : There are a lot of types, in all of them we can have +, -, /,*,% to make basic operations, just remember that if they are integers the result will be integers, and that it will fail if they are not from the same type. :
- The *zero value* is 0 in all types.
- The *int* it is the default type for numbers. 
    - Signed : is at least 32 bits, from 8 bit `int8` to 64 bits `int64`.
    - Unsigned : We can have a especific number of bytes, for example using `uint8` for 8 bits integers, `uint16` for 16 bits and up to `uint32` for 32 bits. 
    - Bit Operations : We can also have bit operators &,|,^,&^,<<,>> which treat numbers as binaries.
- The *float32/64*, the same as integers but with decimals, 64 is the default and can contain much larger decimals. We can have decimal, exponential or mixed values. 
- The *complex64/128*, the same as float but can contain complex numbers, the `complex64` contains float32 and `complex128` contains float 64. We can also make aritmetic operations with them. we can use `complex()`, `real()` and `imag()` to only get a the part of the value we need.

**Text**
- The *string* , is any utf8 character, we can treat them sort of an array, is we specify the letter using `variable[num]` we get a uint8, it is the value assigned to the letter. Strings are generally inmutable but we can concatenate them, something like this `fmt.Printf("%v\n", var1 + var2)`. We can also use `[]byte(var1)` for getting the uint8 value of all the characters in a string.
- The *rune*, is utf32 , if you write a single character between '' instead of "", it becomes a rune  which is an alias to int32.


