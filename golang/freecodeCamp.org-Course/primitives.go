package main

import (
	"fmt"
)

func main() {
	var n bool = true // Initializing variable
	fmt.Printf("%v. %T\n", n, n)

	n = 1 == 1 // this is a logical test
	m := 1 == 2
	fmt.Printf("%v. %T\n", n, n)
	fmt.Printf("%v. %T\n", m, m)

	var o bool
	fmt.Printf("%v. %T\n", o, o) // It has a value zero, in boolean it is false

	a := 10
	b := 3
	// Math opearations
	fmt.Println(a + b) // Addition
	fmt.Println(a - b) // Substraction
	fmt.Println(a / b) // Division but it does not incluse the remainder, it just returns and integer because it is integer by integer division
	fmt.Println(a * b) // Multiplication
	fmt.Println(a % b) // Remainder operator, we get the ramainder from the division

	// Bit operations
	// They depend from the binary of the value of the variables
	// a = 1010
	// b = 0011
	fmt.Println(a & b)  // And operator // The result depends if both values on the binaries are the same, in this case 0010
	fmt.Println(a | b)  // Or operator  // If one ore the other is set to 1, in this case 1011
	fmt.Println(a ^ b)  // Exclusive or operator // Either one has the bit set or the other but not both, in this case 1001
	fmt.Println(a &^ b) // And not operator // If only one of the numbers do not have the bit set, in this case 0100

	a = 8
	// a = 2³
	fmt.Println(a << 3) // Its gonna bit shift a 3 places to the left, in this case, add 3 to the exponent = 2⁶
	fmt.Println(a >> 3) // Its gonna bit shift a 3 places to the right, in this case, remove 3 to the exponent = 2⁰

	var c complex64 = 1 + 2i // Contain complex numbers
	//	var c complex64 = complex(1,2) can also be expressed as this
	fmt.Printf("%v. %T\n", c, c)
	fmt.Printf("%v. %T\n", real(c), real(c)) // Just real part
	fmt.Printf("%v. %T\n", imag(c), imag(c)) // Jus imaginary part

	d := "I am a string"               // A string xd
	fmt.Printf("%v. %T\n", d, d)       // We get the string
	fmt.Printf("%v. %T\n", d[2], d[2]) // We get uint8 number from the 2 letter.
	d2 := "I am a secund string"
	fmt.Printf("%v. %T\n", d+d2, d+d2) // We can concatenate strings

	d3 := []byte(d) // Convert Strings cha racters to bytes
	fmt.Printf("%v. %T\n", d3, d3)

	var r rune = 'a' // Rune is just an alias to int32 so you will get the value of the character in int32
	fmt.Printf("%v. %T\n", r, r)
}
