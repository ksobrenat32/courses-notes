#!/usr/bin/env python3

# Exercise 1: Write a function called chop that takes a list and modifies
# it, removing the first and last elements, and returns None. Then write
# a function called middle that takes a list and returns a new list that
# contains all but the first and last elements.

lst = [1,2,3,4]

def chop(listWork):
    listWork.pop()
    listWork.pop(0)
    return

def middle(listWork):
    listWork.pop()
    listWork.pop(0)
    return listWork

