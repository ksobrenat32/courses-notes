#!/usr/bin/env python3

# Write a program to read through the mail box data and when you find
# line that starts with “From”, you will split the line into words using the
# split function. We are interested in who sent the message, which is the
# second word on the From line.

fname = "mbox-short.txt"
fh = open(fname)
count = 0

for line in fh:
    if line.startswith('From'):
      sline = line.split()
      if len(sline) > 2:
        print(sline[1])
        count = count + 1

print("There were", count, "lines in the file with From as the first word")
