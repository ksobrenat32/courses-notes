#!/usr/bin/env python3

# Write a program that reads the words in words.txt and stores them as
# keys in a dictionary. It doesn’t matter what the values are. Then you
# can use the in operator as a fast way to check whether a string is in the
# dictionary.

fname = "words.txt"
fh = open(fname)
dct = dict()
count = 0

for line in fh:
    wordlst = line.split()
    for word in wordlst:
        dct[word] = count
        count +=1

w2search = input('Word to search: ')

if w2search in dct:
    print(dct[w2search]) 
else:
    print('Not in dictionary')

